﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDetect : MonoBehaviour
{
    public bool isGrounded;
	// Start is called before the first frame update
    void Awake()
    {
     isGrounded = true;   
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(isGrounded);
    }
	public void SwitchState()
	{
		if (isGrounded)
		{
			isGrounded = false;
		}
		else{
			isGrounded = true;
		}
	}
}
