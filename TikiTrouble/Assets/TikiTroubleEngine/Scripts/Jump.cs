﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
	[Range(1,100)]
	public float jumpVelocity;
	private bool isGrounded;
    // Start is called before the first frame update
    void Awake()
    {
        isGrounded = false; 
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown ("Jump")&&isGrounded){
			GetComponent<Rigidbody>().velocity = Vector3.up * jumpVelocity;
		}
    }
	public void SwitchState()
	{
		if (isGrounded)
		{
			isGrounded = false;
		}
		else{
			isGrounded = true;
		}
	}
}
